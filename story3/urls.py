from django.urls import path, include
from . import views

app_name = 'story3'
urlpatterns = [
    path('', views.index, name="index"),
    path('about', views.about, name="about"),
    path('experience', views.experience, name="experience"),
]