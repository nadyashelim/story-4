from django.urls import path, include
from . import views

app_name = 'story7'
urlpatterns = [
    path('', views.accordion, name="accordion"),
]