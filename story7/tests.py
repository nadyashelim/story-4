from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from .views import accordion

# Create your tests here.
class Story7UnitTest(TestCase):

    def test_url_story7(self):
        response = Client().get('/story7/')
        self.assertEquals(200, response.status_code)
    
    def test_template(self):
        response = Client().get('/story7/')
        self.assertTemplateUsed(response, 'accordion.html')
    
    def test_accordion_using_func(self):
        found = resolve('/story7/')
        self.assertEqual(found.func, accordion)
    
    def test_accordion_views(self):
        request = HttpRequest()
        response = accordion(request)
        isi_html = response.content.decode('utf8')
        self.assertIn('Current Activities', isi_html)
        self.assertIn('Organizations Experiences', isi_html)
        self.assertIn('Achievements', isi_html)
        self.assertIn('Commitee Experiences', isi_html)
