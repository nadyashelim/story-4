from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from .views import buku, data

# Create your tests here.
class Story8UnitTest(TestCase):

    def test_url_book(self):
        response = Client().get('/books/')
        self.assertEquals(200, response.status_code)
    
    def test_template_books(self):
        response = Client().get('/books/')
        self.assertTemplateUsed(response, 'books.html')
    
    def test_books_using_func(self):
        found = resolve('/books/')
        self.assertEqual(found.func, buku)

    def test_booksdata_using_func(self):
        found = resolve('/books/data/')
        self.assertEqual(found.func, data)
    
    def test_books_views(self):
        response = Client().get('/books/')
        isi_html = response.content.decode('utf8')
        self.assertIn('<i class="fas fa-search"></i>', isi_html)
        self.assertIn('<input id="keyword" placeholder="Enter keyword">', isi_html)