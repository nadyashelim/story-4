from django.urls import path, include
from . import views

app_name = 'books'
urlpatterns = [
    path('', views.buku, name="buku"),
    path('data/', views.data, name="data"),
]