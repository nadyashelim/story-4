from django.test import TestCase
from django.test import LiveServerTestCase, TestCase, tag, Client
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.urls import reverse, resolve
from django.http import HttpRequest
from . import views
from django.contrib.auth.models import User

# Create your tests here.
class Story9UnitTest(TestCase):
    def test_url_exist(self):
        response = Client().get("/accounts/signup")
        self.assertEquals(200, response.status_code)

    def test_template_login_used(self):
        response = Client().get("/accounts/login")
        self.assertTemplateUsed(response, "registration/login.html")
    
    def test_template_signup_used(self):
        response = Client().get("/accounts/signup")
        self.assertTemplateUsed(response, "registration/signup.html")
    
    def test_form_validation_accepted(self):
        self.credentials = {
            'username': 'pepewe',
            'password': 'lalala'}
        User.objects.create_user(**self.credentials)
        form = AuthenticationForm(data=self.credentials)
        self.assertTrue(form.is_valid())

    def test_signup_page_view_name(self):
        response = self.client.get(reverse('login'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, template_name='registration/login.html')

class UserCreationFormTest(TestCase):

    def test_form(self):
        data = {
            'username': 'pppwww',
            'password1': 'lalalala',
            'password2': 'lalalala',
        }

        form = UserCreationForm(data)
        self.assertFalse(form.is_valid())