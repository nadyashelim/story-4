from django.urls import path, include
from . import views

app_name = 'story1'
urlpatterns = [
    path('', views.home, name="home"),
]