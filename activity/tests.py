from django.test import TestCase, Client, LiveServerTestCase
from django.urls import reverse, resolve
from selenium import webdriver
from django.http import HttpRequest
from activity.models import ModelKegiatan, ModelPartisipan
from .forms import PartisipanForm, KegiatanForm
from .views import add_participant, add_url_test

# Create your tests here.
class Story6UnitTest(TestCase):

    def test_url_add_activity(self):
        response = Client().get('/activity/')
        self.assertEquals(200, response.status_code)
    
    def test_template_add_activity(self):
        response = Client().get('/activity/')
        self.assertTemplateUsed(response, 'add_activity.html')
    
    def test_content_add_activity(self):
        response = Client().get('/activity/')
        isi_html = response.content.decode('utf8')

        self.assertIn('ADD ACTIVITY !', isi_html)
        self.assertIn('<input type="submit" value="Submit" class="btn btn-primary click">', isi_html)
        self.assertIn('<form action="" method="POST" novalidate>', isi_html)  

    def test_content_view_activity(self):
        response = Client().get('/activity/viewactivity')
        isi_html = response.content.decode('utf8')

        self.assertIn('Here are some of my activities :)', isi_html)
        self.assertIn('Activity', isi_html)
        self.assertIn('Participant', isi_html)
        self.assertIn('<input type="submit" style="background-color: salmon; color: white;" value="Join" class="btn click">', isi_html)
    
    def test_activity_model(self):
        ModelKegiatan.objects.create(activity="tes")
        jumlah_data = ModelKegiatan.objects.all().count()
        self.assertEquals(jumlah_data, 1)
    
    def test_model_name(self):
        ModelKegiatan.objects.create(activity='ModelKegiatan object (1)')
        kegiatan = ModelKegiatan.objects.get(activity='ModelKegiatan object (1)')
        self.assertEqual(str(kegiatan),'ModelKegiatan object (1)')
    
    def test_model_relational(self):
        ModelKegiatan.objects.create(activity = "pwpw")
        kegiatan_id = ModelKegiatan.objects.all()[0].id
        kegiatan_di_id = ModelKegiatan.objects.get(id=kegiatan_id)
        ModelPartisipan.objects.create(join_activity = kegiatan_di_id, Name="aa")
        jumlah = ModelPartisipan.objects.filter(join_activity=kegiatan_di_id).count()
        self.assertEquals(jumlah, 1)
    
    def test_model_name2(self):
        ModelKegiatan.objects.create(activity='ya')
        idpk = ModelKegiatan.objects.all()[0].id
        kegiatan = ModelKegiatan.objects.get(id=idpk)
        ModelPartisipan.objects.create(join_activity=kegiatan, Name='hmm')
        peserta = ModelPartisipan.objects.get(join_activity=kegiatan)
        self.assertEqual(str(peserta),'ModelPartisipan object (1)')

class DataUnitTest(TestCase):
    def test_url_view_activity(self):
        response = Client().get('/activity/')
        self.assertEquals(200, response.status_code)
    
    def test_template_view_activity(self):
        response = Client().get('/activity/viewactivity')
        self.assertTemplateUsed(response, 'view_activity.html')

    def test_model_create(self):
        kegiatan1 = ModelKegiatan.objects.create(activity="yeehaw")
        ModelPartisipan.objects.create(join_activity = kegiatan1, Name="aku")
        jumlah = ModelPartisipan.objects.all().count()
        self.assertEquals(jumlah, 1)


    def test_data_view_show(self):
        ModelKegiatan.objects.create(activity='Test')
        request = HttpRequest()
        response = add_participant(request)
        isi_html = response.content.decode('utf8')
        self.assertIn('Test', isi_html)
    
    def test_insert_blank_partisipan(self):
        kegiatan_new = ModelKegiatan.objects.create(activity = "ppw")
        form = PartisipanForm(data = {'kegiatan' :kegiatan_new, 'nama' : ''})
        self.assertEqual(form.errors['Name'], ["This field is required."])