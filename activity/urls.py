from django.urls import path, include
from . import views

app_name = 'activity'
urlpatterns = [
    path('', views.add_url_test, name="add_activity"),
    path('viewactivity', views.add_participant, name="view_activity"),
]