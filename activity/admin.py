from django.contrib import admin
from .models import ModelKegiatan, ModelPartisipan

# Register your models here.
admin.site.register(ModelKegiatan)
admin.site.register(ModelPartisipan)