from django import forms

from .models import ModelKegiatan, ModelPartisipan

class KegiatanForm(forms.ModelForm):
    class Meta:
        model = ModelKegiatan
        fields = ['activity']

        widgets = {
            'activity':forms.TextInput(attrs={'class': 'form-control'}),
        }

class PartisipanForm(forms.ModelForm):
    class Meta:
        model = ModelPartisipan
        fields = ['Name', 'join_activity']

        widgets = {
            'Name':forms.TextInput(attrs={'class': 'form-control'}),
        }
