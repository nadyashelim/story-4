from django.db import models

# Create your models here.
class ModelKegiatan(models.Model):
    activity = models.CharField(max_length=60, null=True, blank=False)
    
    def __str__(self):
        return self.activity

class ModelPartisipan(models.Model):
    Name = models.CharField('Partisipan',max_length=60, null=True, blank=False)
    join_activity = models.ForeignKey(ModelKegiatan, on_delete=models.CASCADE, null=True, blank=False)