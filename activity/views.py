from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import ModelKegiatan, ModelPartisipan
from .forms import KegiatanForm, PartisipanForm

# Create your views here.
def add_url_test(request):
    data_kegiatan = ModelKegiatan.objects.all()
    form = KegiatanForm()
    if request.method == 'POST':
        forminput = KegiatanForm(request.POST)
        if forminput.is_valid():
            forminput.save()
            return render(request, 'add_activity.html', {'form':form, 'data' : data_kegiatan})
            
    else:
        data = ModelKegiatan.objects.all()
        return render(request, 'add_activity.html', {'form':form, 'data':data})

def add_participant(request):
    form = PartisipanForm();
    data_kegiatan = ModelKegiatan.objects.all()
    data_partisipan = ModelPartisipan.objects.all()
    if request.method == 'POST':
        forminput = PartisipanForm(request.POST)
        if forminput.is_valid():
            forminput.save()
            return redirect('activity:view_activity')
        else:
            dataPartisipan = ModelPartisipan.objects.all()
            return render(request, 'view_activity.html', {'form':form, 'status': 'failed', 'Partisipan' : data_partisipan, 'Kegiatan': data_kegiatan,})
    else:
        return render(request,'view_activity.html', {'form':form, 'status': 'failed', 'Partisipan' : data_partisipan, 'Kegiatan': data_kegiatan})