from django.shortcuts import render, redirect
from .models import Jadwal
from .forms import MatkulForm

# Create your views here.
def add_matkul(request):
    if request.method == 'POST':
        form = MatkulForm(request.POST)
        if form.is_valid():
            jadwal = form.save()
            jadwal.save()
            return redirect('jadwal:view_matkul')
    else:
        form = MatkulForm()
    
    return render(request, 'add_matkul.html', {'form': form})

def delete_jadwal(request, id):
    Jadwal.objects.get(id=id).delete()
    return redirect('jadwal:view_matkul')

def view_matkul(request):
    semua_jadwal = Jadwal.objects.order_by('Subject')
    return render(request, 'view_matkul.html', {'semua_jadwal': semua_jadwal})

def detail(request, id):
    detail = Jadwal.objects.get(id=id)
    return render(request, 'detail.html', {'detail_matkul': detail})