from django.db import models

# Create your models here.
class Jadwal(models.Model):
    Subject = models.CharField(max_length=50)
    Lecturer = models.CharField(max_length=50)
    Credits = models.CharField(max_length=50)
    Description = models.TextField(max_length=100)
    Semester = models.CharField(max_length=50)
    Room = models.CharField(max_length=50, default="Online Class")