from django.urls import path, include
from . import views

app_name = 'jadwal'
urlpatterns = [
    path('', views.add_matkul, name="add_matkul"),
    path('view_matkul', views.view_matkul, name="view_matkul"),
    path('delete/<id>', views.delete_jadwal, name="delete_matkul"),
    path('detail/<id>', views.detail, name="detail"),
]