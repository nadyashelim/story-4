from django import forms

from .models import Jadwal

class MatkulForm(forms.ModelForm):
    class Meta:
        model = Jadwal
        fields = ['Subject', 'Lecturer', 'Credits', 'Description', 'Semester', 'Room']

        widgets = {
            'Subject':forms.TextInput(attrs={'class': 'form-control'}),
            'Lecturer':forms.TextInput(attrs={'class': 'form-control'}),
            'Credits':forms.TextInput(attrs={'class': 'form-control'}),
            'Description':forms.TextInput(attrs={'class': 'form-control'}),
            'Semester':forms.TextInput(attrs={'class': 'form-control'}),
            'Room':forms.TextInput(attrs={'class': 'form-control'}),
        }